<?php
namespace Maksoft\Amount;
bcscale(2);


class Money
{
    private $coef;
    private $cash_desk;
    private $currency;
    private $symbol;
    public $amount="";
    private $converted = false;

    public function init($amount, $currency, $symbol)
    {
        $this->amount = $amount;
        $this->amount = $this->format();
        $this->currency = $currency;
        $this->symbol = $symbol;
    }

    private function format($decimal_places=3)
    {
        return number_format($this->amount, $decimal_places, ".", "");
    }

    public function getCurrencySymbol()
    {
        return $this->symbol;
    }

    public function __toString()
    {
        return $this->format(2).$this->symbol;
    }

    public static function BG($amount)
    {
        $symbol = "лв.";
        $m = new Money();
        $m->init($amount, "BG", $symbol);
        return $m;
    }

    public static function EUR($amount)
    {
        $symbol = "€";
        $m = new Money();
        $m->init($amount, "EUR", $symbol);
        return $m;
    }

    public static function USD($amount)
    {
        $symbol = "$";
        $m = new Money();
        $m->init($amount, "USD", $symbol);
        return $m;
    }

    public function getCurrency()
    {
        return $this->currency;
    }

    public function getAmount()
    {
        return $this->format();
    }

    public function mul($mul)
    {
        $this->amount = bcmul($this->amount, $mul);
    }

    public function div($dividor)
    {
        $this->amount = bcdiv($this->amount, $dividor);
    }

    public function add(Money $other)
    {
        $this->sameCurrency($other);
        $this->amount = bcadd((string) $this->amount, (string) $other->getAmount());
    }

    protected function sameCurrency(Money $other)
    {
        if($this->currency !== $other->getCurrency()){
            throw new \Exception("Аритметичните операции с различни валути са забранени", 1111);
        }
    }

    public function sub(Money $other)
    {
        $this->sameCurrency($other);
        $this->amount = bcsub($this->amount, $other->getAmount());
    }

    public function eq(Money $other)
    {
        $this->sameCurrency($other);
        $o = bccomp($this->amount, $other->getAmount());
        if($o===0){
            return True;
        }
        return False;
    }
}
