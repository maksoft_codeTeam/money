<?php
require __DIR__ . "/../vendor/autoload.php";


class TestMoney extends PHPUnit_Framework_TestCase
{
    public function __construct()
    {
        $this->cash = Maksoft\Amount\Money::BG(10.20);
    }

    public function test_add()
    {
        $other = Maksoft\Amount\Money::BG(22.360000012);
        $this->cash->add($other);
        $this->assertEquals((string) $this->cash, 32.56."лв.");
    }

    public function test_add_three_peaces_product_price()
    {
        $other = Maksoft\Amount\Money::BG(0.126);
        $other->mul(1000);
        $this->assertEquals("126.00лв.", (string) $other);
    }

    public function test_sub()
    {
        $other = Maksoft\Amount\Money::BG(22.36);
        $this->cash->sub($other);
        $this->assertEquals((string) $this->cash, -12.16.'лв.');
    }

    public function test_eq()
    {
        $other = Maksoft\Amount\Money::BG(10.20);
        $this->assertTrue($this->cash->eq($other));
    }

    public function test_mul()
    {
        $this->cash->mul(10);
        $this->assertEquals("102.00лв.", (string) $this->cash);
    }

    public function test_mul_incorect_multiplier()
    {
        $this->cash->mul("asd");
        $this->assertEquals((string) $this->cash, "0.00лв.");
        $this->cash->mul("!@^#&%");
        $this->assertEquals((string) $this->cash, "0.00лв.");
        $this->cash->mul("-2313.asda");
        $this->assertEquals((string) $this->cash, "0.00лв.");
        $this->cash->mul("-2313.123");
        $this->assertEquals((string) $this->cash, "0.00лв.");
        try{
            $this->cash->mul(array());;
        } catch (\Exception $e) {
            $code = $e->getCode();
        }
        $this->assertEquals(1112, $code);
    }

    public function test_div()
    {
        $this->cash->div(10);
        $this->assertEquals("1.02лв.", (string) $this->cash);
    }

    public function test_toString()
    {
        $this->assertEquals((string) $this->cash, "10.20лв.");
    }

    public function test_add_different_currencies()
    {
        $other = Maksoft\Amount\Money::EUR(22.36);
        $code = 0;
        try{
            $this->cash->add($other);
        } catch (Exception $e){
            $code = $e->getCode();
        }
        $this->assertEquals(1111, $code);
    }

    public function test_sub_different_currencies()
    {
        $other = Maksoft\Amount\Money::EUR(22.36);
        $code = 0;
        try{
            $this->cash->sub($other);
        } catch (Exception $e){
            $code = $e->getCode();
        }
        $this->assertEquals(1111, $code);
    }

    public function test_eq_different_currencies()
    {
        $other = Maksoft\Amount\Money::EUR(22.36);
        $code = 0;
        try{
            $this->cash->sub($other);
        } catch (Exception $e){
            $code = $e->getCode();
        }
        $this->assertEquals(1111, $e->getCode());
    }

}
